{:repl {:plugins [[cider/cider-nrepl "0.19.0-SNAPSHOT"]
                  [refactor-nrepl "2.4.0"]]}
 :user {:plugins [[lein-ancient "0.6.15"]
                  [lein-count "1.0.9"]]
        :dependencies [[vvvvalvalval/scope-capture "0.1.4"]]
        :injections [(require 'sc.api)]}}
